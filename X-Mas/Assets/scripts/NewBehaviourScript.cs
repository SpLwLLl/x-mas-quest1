using UnityEngine;

public class MoveObject : MonoBehaviour
{
    public Transform targetPosition; // Публичная переменная для указания пустого объекта, куда нужно переместить

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("hat"))
        {
         transform.position = targetPosition.position;
        }
    }
}