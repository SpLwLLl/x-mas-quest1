using UnityEngine;

public class TriggerRotation : MonoBehaviour

{
    public float timeRemaining = 2;
    private void OnTriggerEnter(Collider other)
    {
        // Проверяем, является ли касающийся объект пустышкой
        if (other.CompareTag("Player"))
        {
            // Поворачиваем текущий GameObject на 45 градусов вокруг оси Y
            transform.Rotate(0, 45, 0);
        }
    }
    void Update()
    {
        if (timeRemaining <= 0)
        {
            transform.Rotate(0, -45, 0);
        }
    }
}