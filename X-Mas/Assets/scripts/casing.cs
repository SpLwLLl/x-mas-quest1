using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class casing : MonoBehaviour
{
    public GameObject GameObject;
    public Button button;
    public float speed = 1f;


    // Update is called once per frame

    private void OnTriggerEnter(Collider other)
    {
        // ���������, �������� �� ������, �������� � �������, �������
        if (other.CompareTag("Player"))
        {
            // ���������� ������
            button.gameObject.SetActive(true);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        // ���������, �������� �� ������, ��������� �� ��������, �������
        if (other.CompareTag("Player"))
        {
            // �������� ������
            button.gameObject.SetActive(false);
        }
    }
    private void Update()
    {
        // ��������� ����� ������� ������� � ������ ����������� �� ��� X
        Vector3 newPosition = transform.position + transform.forward * speed * Time.deltaTime;

        // ���������� ������ �� ����� �������
        transform.position = newPosition;
    }
}

