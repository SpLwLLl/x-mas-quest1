using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System;

public class key : MonoBehaviour, IInteractable
{
    public bool isKeyed;
    public float keys = 0;
    public TMP_Text interactionText;

    void InteractionRay()
    { 
     interactionText.text = keys.ToString();
    }

    void Start()
    {
        interactionText.text = ((int)keys).ToString();
    }

    void UpdateHpText()
    {
        interactionText.text = ((int)keys).ToString();
    }
    void FixedUpdate()
    {
        UpdateHpText();
    }
    public string GetDescription()
    {
        if (isKeyed) return "Press [E] to take the key";
        return "Press [E] to take the key";
    }

    public void Interact()
    {
        isKeyed = !isKeyed;
        if (isKeyed)
        {
            keys += 1;
            Destroy(gameObject); 
        }

    }
}
