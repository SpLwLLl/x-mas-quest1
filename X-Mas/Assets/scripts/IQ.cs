﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class IQ : MonoBehaviour
{
    public Transform Player;
    float distance;
    NavMeshAgent myAgent;
    Animator myAnim;
    public float hp = 100;


    private void Awake()
    {
        if (Player == null)
        {
            Player = GameObject.FindWithTag("Player").transform;
        }
    }

    void Start()

    {
        myAgent = GetComponent<NavMeshAgent>();
        myAnim = GetComponent<Animator>();
    }

    private void Update()
    {

        distance = Vector3.Distance(transform.position, Player.position);
        // Debug.Log(distance);
        if (distance > 10)
        {
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", true);
            myAnim.SetBool("ATTAKS", false);
            myAnim.SetBool("RUN", false);
        }
        if(distance <= 10)
        {
            myAgent.enabled = true;
            myAgent.SetDestination(Player.position);
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTAKS", false);
            myAnim.SetBool("RUN", true);
        }
        if (distance <= 1.5)
        {
            myAgent.enabled = false;
            myAnim.SetBool("IDLE", false);
            myAnim.SetBool("ATTAKS", true);
            myAnim.SetBool("RUN", false);
        }

    }
}
